#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <SFML/Window/Keyboard.hpp>
#include "Collision.h"
#include "Collision.cpp"
#include "Button.h"
#include <iostream>
#include <cmath>
#include <sstream>
#include <cstdlib>
#include <ctime>
#include <string>

struct weapons {
        int damage , capacityt0 , capacityt1 , xpo , ypo ;
        sf::Texture texture1 , texturelock ;
        sf::Sprite sprite1 ;
        bool pickt0 = true , pickt1 = true ;
        std ::string name ;
};

void unlockifitschoosable (sf::Texture &t , sf::Sprite &s , bool);

void delay(int delayTime);
int main()
{
    bool gameStart=false;
    while(true)
    {
    if(!gameStart)
    {
    sf::RenderWindow menu(sf::VideoMode(1600,900),"MENU",sf::Style::Titlebar | sf::Style::Close);

    sf::Music mus;
    if (!mus.openFromFile("images/TrapTheme0.wav"))
        return -1;
    mus.play();
    mus.setLoop(true);

    sf::Texture redTankLogoTex;
    sf::Texture greenTankLogoTex;
    sf::Texture backdropTex;

    if(!redTankLogoTex.loadFromFile("images/redTankLogo.png"))
        return EXIT_FAILURE;
    if(!greenTankLogoTex.loadFromFile("images/greenTankLogo.png"))
        return EXIT_FAILURE;
    if(!backdropTex.loadFromFile("images/LSD.jpg"))
        return EXIT_FAILURE;

    sf::Sprite backdrop(backdropTex);
    backdrop.setScale(0.95,0.84);

    sf::Sprite redTankLogo(redTankLogoTex);
    redTankLogo.setPosition(50,500);
    sf::Sprite greenTankLogo(greenTankLogoTex);
    greenTankLogo.setScale(-1,1);
    greenTankLogo.setPosition(1550,500);

    sf::Font neon;
    neon.loadFromFile("images/Monoton-RXOM.ttf");
    sf::Text title1,title2;
    title1.setFont(neon);
    title2.setFont(neon);
    title1.setFillColor(sf::Color(172,255,110));
    title2.setFillColor(sf::Color(255,77,36));
    title1.setCharacterSize(200);
    title2.setCharacterSize(200);
    title1.setOutlineThickness(30);
    title2.setOutlineThickness(30);
    title1.setOutlineColor(sf::Color(19,60,176));
    title2.setOutlineColor(sf::Color(19,60,176));
    title1.setPosition(50,50);
    title2.setPosition(750,50);
    title1.setString("ACID");
    title2.setString("TANKS");

    Button btn1("PLAY",{320,100},100,sf::Color::Transparent,sf::Color::White);
    btn1.setFont(neon);
    btn1.setPosition({650,500});

    Button btn2("LAN",{320,100},100,sf::Color::Transparent,sf::Color::White);
    btn2.setFont(neon);
    btn2.setPosition({650,600});

    Button btn3("EXIT",{320,100},100,sf::Color::Transparent,sf::Color::White);
    btn3.setFont(neon);
    btn3.setPosition({650,700});

    sf::Text creators;
    creators.setFont(neon);
    creators.setCharacterSize(20);
    creators.setFillColor(sf::Color::White);
    creators.setOutlineColor(sf::Color::Black);
    creators.setOutlineThickness(5);
    creators.setString("CREATED  BY  AMIRALI.PAK  &  PARSA.EISAZADEH");
    creators.setPosition(5,875);

    while (menu.isOpen())
    {
        sf::Event event;
        while (menu.pollEvent(event))
        {
            switch(event.type)
            {
                case(sf::Event::MouseMoved):
                    if(btn1.isMouseOver(menu))
                        btn1.setTextOutline(5,sf::Color(19,60,176));
                    else
                        btn1.setTextOutline(0,sf::Color::Transparent);
                    if(btn2.isMouseOver(menu))
                        btn2.setTextOutline(5,sf::Color(19,60,176));
                    else
                        btn2.setTextOutline(0,sf::Color::Transparent);
                    if(btn3.isMouseOver(menu))
                        btn3.setTextOutline(5,sf::Color(19,60,176));
                    else
                        btn3.setTextOutline(0,sf::Color::Transparent);
                    break;
                case(sf::Event::MouseButtonPressed):
                    if(btn1.isMouseOver(menu))
                    {
                        gameStart=true;
                        mus.stop();
                        mus.setLoop(false);
                        menu.close();
                    }
                    else if(btn3.isMouseOver(menu))
                    {
                        menu.close();
                        return EXIT_SUCCESS;
                    }
                    break;
                case(sf::Event::Closed):
                    menu.close();
                    return EXIT_SUCCESS;
                    break;
                default:
                    break;
            }
        }
        menu.clear();
        menu.draw(backdrop);
        menu.draw(redTankLogo);
        menu.draw(greenTankLogo);
        menu.draw(title1);
        menu.draw(title2);
        btn1.drawTo(menu);
        btn2.drawTo(menu);
        btn3.drawTo(menu);
        menu.draw(creators);
        menu.display();
    }
    }
    if(gameStart)
    {
    sf::Font font;
    font.loadFromFile("images/Monoton-RXOM.ttf");
    sf::Sprite ban ;
    sf::Texture trumpModeBackDrop ;
    sf::Texture trumpModeEndScreen ;
    sf::Music trumpModeMusic ;

    sf::Font digitalfont;
    digitalfont.loadFromFile("images/digital-7.ttf");
    //Music
    sf::Music mus;
    if (!mus.openFromFile("images/TrapTheme.wav"))
        return -1;
    mus.play();
    mus.setLoop(true);

    // Create the main window
    sf::RenderWindow app(sf::VideoMode(1600, 900)," <<<<ACID~TANKS>>>> ");
    //set window icon
    sf::Image icon;
    icon.loadFromFile("images/icon.png");
    app.setIcon(icon.getSize().x, icon.getSize().y, icon.getPixelsPtr());

    // Load a sprite to display
    //define textures
    sf::Texture backdropTex;
    sf::Texture hillTex;

    sf::Texture redTex;
    sf::Texture red1Tex;
    sf::Texture red2Tex;
    sf::Texture red3Tex;
    sf::Texture red4Tex;

    sf::Texture redLoolTex;
    sf::Texture blueTex;
    sf::Texture blue1Tex;
    sf::Texture blue2Tex;
    sf::Texture blue3Tex;
    sf::Texture blue4Tex;


    sf::Texture blueLoolTex;
    sf::Texture bdot;
    sf::Texture wdot;
    sf::Texture simpBul;
    sf::Texture explosion1;
    sf::Texture acidexplosion;
    sf::Texture discoballexplosion;
    sf::Texture endexplosion;
    sf::Texture endscreen;
    sf::Texture powerbacktex;
    sf::Texture powerbartex;
    sf::Texture powerbarshadowtex;
    sf::Texture bulicontex;
    sf::Texture aimicontex;
    sf::Texture metalhub;
    sf::Texture windIconTex;
    sf::Texture trumpHills;

    sf::CircleShape aimback(45);
    aimback.setFillColor(sf::Color::White);
    aimback.setOutlineThickness(5);
    aimback.setOutlineColor(sf::Color::Black);
    aimback.setPosition(573,780);

    sf::Text powerValue;
    powerValue.setFont(digitalfont);
    powerValue.setFillColor(sf::Color::Black);
    powerValue.setPosition(105,690);
    powerValue.setCharacterSize(70);

    sf::Text turnIndicator;
    turnIndicator.setFont(digitalfont);
    turnIndicator.setPosition(75,780);
    turnIndicator.setCharacterSize(80);
    turnIndicator.setOutlineThickness(5);
    turnIndicator.setOutlineColor(sf::Color::Black);

    sf::Text angleValue;
    angleValue.setFont(digitalfont);
    angleValue.setOrigin(angleValue.getLocalBounds().width /2 , angleValue.getLocalBounds().height /2);
    angleValue.setFillColor(sf::Color::Black);
    angleValue.setCharacterSize(70);
    angleValue.setPosition(586,778);

    sf::Text windValue;
    windValue.setFont(font);
    windValue.setFillColor(sf::Color::White);
    windValue.setCharacterSize(70);
    windValue.setPosition(788,0);

    sf::Texture backColor ;
    sf::Sprite highlight ;
    //load textures
    if (!windIconTex.loadFromFile("images/wind.png"))
        return EXIT_FAILURE;
    if (!backColor.loadFromFile("images/highlight2.png"))
        return EXIT_FAILURE;
    if (!powerbarshadowtex.loadFromFile("images/powerbarshadow2.png"))
        return EXIT_FAILURE;
    if (!metalhub.loadFromFile("images/hub.jpg"))
        return EXIT_FAILURE;
    if (!aimicontex.loadFromFile("images/aim.png"))
        return EXIT_FAILURE;
    if (!bulicontex.loadFromFile("images/missile.png"))
        return EXIT_FAILURE;
    if (!powerbartex.loadFromFile("images/powerbar2.png"))
        return EXIT_FAILURE;
    if (!powerbacktex.loadFromFile("images/metalplate.png"))
        return EXIT_FAILURE;
    if (!endscreen.loadFromFile("images/end.jpg"))
        return EXIT_FAILURE;
    if (!endexplosion.loadFromFile("images/endexplosion.png"))
        return EXIT_FAILURE;
    if (!explosion1.loadFromFile("images/explosion2.png"))
        return EXIT_FAILURE;
    if (!acidexplosion.loadFromFile("images/acidexplosion.png"))
        return EXIT_FAILURE;
    if (!discoballexplosion.loadFromFile("images/discoballexplosion.png"))
        return EXIT_FAILURE;
    if (!bdot.loadFromFile("images/blackdot.png"))
        return EXIT_FAILURE;
    if (!wdot.loadFromFile("images/whitedot.png"))
        return EXIT_FAILURE;
    if (!backdropTex.loadFromFile("images/backdrop.jpg"))
        return EXIT_FAILURE;
    if (!hillTex.loadFromFile("images/hills.png"))
        return EXIT_FAILURE;
    if (!trumpHills.loadFromFile("images/trumppic.png"))
        return EXIT_FAILURE;
    if (!redTex.loadFromFile("images/redtank.png"))
        return EXIT_FAILURE;
    if (!red1Tex.loadFromFile("images/redtank1.png"))
        return EXIT_FAILURE;
    if (!red2Tex.loadFromFile("images/redtank2.png"))
        return EXIT_FAILURE;
    if (!red3Tex.loadFromFile("images/redtank3.png"))
        return EXIT_FAILURE;
    if (!red4Tex.loadFromFile("images/redtank4.png"))
        return EXIT_FAILURE;
    if (!redLoolTex.loadFromFile("images/redloole.png"))
        return EXIT_FAILURE;
    if (!blueTex.loadFromFile("images/bluetank.png"))
        return EXIT_FAILURE;
    if (!blue1Tex.loadFromFile("images/bluetank1.png"))
        return EXIT_FAILURE;
    if (!blue2Tex.loadFromFile("images/bluetank2.png"))
        return EXIT_FAILURE;
    if (!blue3Tex.loadFromFile("images/bluetank3.png"))
        return EXIT_FAILURE;
    if (!blue4Tex.loadFromFile("images/bluetank4.png"))
        return EXIT_FAILURE;
    if (!blueLoolTex.loadFromFile("images/blueloole.png"))
        return EXIT_FAILURE;
    if (!simpBul.loadFromFile("images/discoball.png"))
        return EXIT_FAILURE;

    //backdrop & hills sprite
    sf::Sprite backdrop(backdropTex);
    backdrop.setScale(0.84,0.84);

    sf::Sprite hills(hillTex);
    //hills.setScale(0.84,0.84);
    //hills.setPosition(0.84,0.84);
    hills.setPosition(-150,350);

    sf::Sprite endScreen(endscreen);

    //hub
    sf::Sprite hub(metalhub);
    hub.setPosition(0,660);

    //hub
    sf::Sprite metalplate(powerbacktex);
    metalplate.setScale(0.7,0.38);
    metalplate.setPosition(15,675);

    sf::Sprite powerBar(powerbartex);
    powerBar.setPosition(55,700);
    sf::Sprite powerbarshadow(powerbarshadowtex);
    powerbarshadow.setPosition(55,693);
    powerbarshadow.setScale(0.7,0.60);

    sf::Sprite bullogo(bulicontex);
    bullogo.setScale(0.20,0.20);
    bullogo.setPosition(10+568,685);

    sf::Sprite aimlogo(aimicontex);
    aimlogo.setColor(sf::Color::Black);
    aimlogo.setScale(0.1,0.1);
    aimlogo.setPosition(575,780);

    sf::Sprite windIcon(windIconTex);
    windIcon.setPosition(800,0);

    sf::Sprite windFlow(windIconTex);


    //blueTank sprites
    const int bluetankxpos=1320;
    const int bluetankypos=380;
    int blueLife=100;

    sf::Sprite blueTank(blueTex);
    blueTank.setScale(0.15,0.2);
    blueTank.setPosition(bluetankxpos,bluetankypos);

    sf::Sprite blueLool(blueLoolTex);
    blueLool.setScale(0.2,0.2);
    blueLool.setPosition(70+bluetankxpos,25+bluetankypos);
    int blueLoolLength=82;

    sf::Sprite mbdot(bdot);// 0.025 , 0.025 , 85 , 45
    mbdot.setScale(0.025,0.025);
    mbdot.setPosition(bluetankxpos+85,45+bluetankypos);

    sf::Sprite rbdot(wdot);
    rbdot.setScale(0.025,0.025);
    rbdot.setPosition(bluetankxpos+143,45+bluetankypos);

    sf::Sprite lbdot(wdot);
    lbdot.setScale(0.025,0.025);
    lbdot.setPosition(bluetankxpos+38,45+bluetankypos);
    //redTank sprites
    const int redtankxpos=100;
    const int redtankypos=380;
    int redLife=100;

    sf::Sprite redTank(redTex);
    redTank.setScale(0.15,0.2);
    redTank.setPosition(redtankxpos,redtankypos);

    sf::Sprite redLool(redLoolTex);
    redLool.setScale(0.2,0.2);
    redLool.setPosition(100+redtankxpos,25+redtankypos);
    int redLoolLength=88;

    sf::Sprite mrdot(bdot);
    mrdot.setScale(0.025,0.025);
    mrdot.setPosition(68+redtankxpos,45+redtankypos);

    sf::Sprite rrdot(wdot);
    rrdot.setScale(0.025,0.025);
    rrdot.setPosition(115+redtankxpos,45+redtankypos);

    sf::Sprite lrdot(wdot);
    lrdot.setScale(0.025,0.025);
    lrdot.setPosition(10+redtankxpos,45+redtankypos);
    //bullet
    sf::Sprite bul;

    //set rotation origin
    redLool.setOrigin(0,33);
    blueLool.setOrigin(411,30);

    //variables
    weapons options [4] ;
    weapons trump ;
    int choose = 0 ;
    int slowmo=1;
    char c ;
    bool trumpmode = false ;
    int damage =20 ;
    bool turn=0;
    bool shootFlag=0;
    double ydrop;
    double basketballydrop;
    bool basketballFlag=0;
    int basketballHits=0;
    int poisonCheck=0;
    int power;
    int PowerR=100;
    int PowerB=100;
    int angle;
    int windeffect;
    bool windFlag=0;
    bool windFlowFlag=0;
    srand(time(NULL));

    //displaying life values
    std::ostringstream ss;
    sf::Text redLifeValue;
    sf::Text blueLifeValue;

    //int to text conversion
    ss.str("");
    ss << redLife;
    redLifeValue.setString(ss.str());
    ss.str("");
    ss << blueLife;
    blueLifeValue.setString(ss.str());

    //modifying text arguments
    redLifeValue.setPosition(80,0);
    blueLifeValue.setPosition(1310,0);
    redLifeValue.setFont(font);
    blueLifeValue.setFont(font);
    redLifeValue.setFillColor(sf::Color(255,77,36));
    blueLifeValue.setFillColor(sf::Color(172,255,110));
    redLifeValue.setCharacterSize(100);
    blueLifeValue.setCharacterSize(100);

    //end text
    sf::Text endtext;
    endtext.setFont(font);
    endtext.setCharacterSize(200);

    //lock textures
    options[1].texturelock.loadFromFile("images/fireballlocked.png");
    options[2].texturelock.loadFromFile("images/acidballlocked.png");
    options[3].texturelock.loadFromFile("images/basketballlocked.png");

    //making the highlighter

    sf::RectangleShape backRect ;
    backRect.setFillColor(sf::Color::Black);
    backRect.setSize(sf::Vector2f(775 , 30 ));
    backRect.setPosition(745 , 665);
    //highlight for selected weapon
    backColor.loadFromFile("images/highlight2.png");
    highlight.setTexture(backColor);
    highlight.setScale(1.05,0.95);
    highlight.setPosition(745,705);

    //define trump
    trumpModeEndScreen.loadFromFile("images/trumpTank.jpg");
    trumpModeBackDrop.loadFromFile("images/noise.jpg");
    sf::Text trumpModeText;
    //trumpModeMusic
    trump.texture1.loadFromFile("images/trump.png");
    trump.sprite1.setTexture(trump.texture1);
    trump.damage = 100 ;
    // back metal plates for weapons
    sf::Texture backDefaultT[4] ;
    sf::Sprite backDefaultS[4];
    for(int i=0 ; i<4;i++){
            backDefaultT[i].loadFromFile("images/metalplate.png");
            backDefaultS[i].setTexture(backDefaultT[i]);
            backDefaultS[i].setScale(0.2 , 0.3);
    }

    backDefaultS[0].setPosition(745 , 705) ;
    backDefaultS[1].setPosition(950 , 705) ;
    backDefaultS[2].setPosition(1150 , 705) ;
    backDefaultS[3].setPosition(1350 , 705) ;

    //defining options of weapons

    //disco ball
    options[0].texture1.loadFromFile("images/discoball.png");
    options[0].sprite1.setTexture(options[0].texture1);
    options[0].xpo = 745 ;
    options[0].ypo = 705 ;
    options[0].sprite1.setPosition(755,710);
    options[0].sprite1.setScale(0.25 , 0.225);
    options[0].damage =50 ;
    options[0].capacityt0= 1000 ;
    options[0].capacityt1 = 1000 ;
    options[0].name = "discoball" ;
    //fireball
    options[1].texture1.loadFromFile("images/fireball.png");
    options[1].sprite1.setTexture(options[1].texture1);
    options[1].xpo = 950 ;
    options[1].ypo = 705 ;
    options[1].sprite1.setPosition(960,720);
    options[1].sprite1.setScale(0.25 , 0.225);
    options[1].damage =20 ;
    options[1].capacityt0 = 2 ;
    options[1].capacityt1= 2 ;
    options[1].name = "fireball" ;
    //acid ball
    options[2].texture1.loadFromFile("images/acidball.png");
    options[2].sprite1.setTexture(options[2].texture1);
    options[2].xpo = 1150 ;
    options[2].ypo = 705 ;
    options[2].sprite1.setPosition(1170,720);
    options[2].sprite1.setScale(0.25 , 0.225);
    options[2].damage =25 ;
    options[2].capacityt0 = 1 ;
    options[2].capacityt1 = 1 ;
    options[2].name = "acid ball" ;
    //basketball
    options[3].texture1.loadFromFile("images/basketball.png");
    options[3].sprite1.setTexture(options[3].texture1);
    options[3].xpo = 1350 ;
    options[3].ypo = 705 ;
    options[3].sprite1.setPosition(1375,725);
    options[3].sprite1.setScale(0.25 , 0.225);
    options[3].damage =24 ;
    options[3].capacityt0 = 2 ;
    options[3].capacityt1 = 2 ;
    options[3].name = "basketball" ;

    for(int i=0 ; i<4;i++){
            backDefaultS[i].setTexture(powerbacktex);
            backDefaultS[i].setScale(0.2 , 0.3);
            backDefaultS[i].setPosition(options[i].xpo , options[i].ypo);
    }
    //display damage , weapon name and weapon capacity
    //weapon name
    sf::Text weaponName ;
    weaponName.setFont(digitalfont);
    weaponName.setPosition(745 , 655);
    weaponName.setFillColor(sf::Color::Green);
    weaponName.setCharacterSize(40);
    weaponName.setString(options[choose].name);
    //damage name
    sf::Text damageName ;
    damageName.setFont(digitalfont);
    damageName.setPosition(1350 , 655);
    damageName.setFillColor(sf::Color::Green);
    damageName.setCharacterSize(40);
    damageName.setString("damage");
    //damage value
    sf::Text damageIcon ;
    damageIcon.setFont(digitalfont);
    damageIcon.setPosition(1475 , 655);
    damageIcon.setFillColor(sf::Color::Green);
    damageIcon.setCharacterSize(40);
    ss.str("");
    ss << options[0].damage ;
    damageIcon.setString(ss.str());
    //capacity value
    sf::Text capacityIcon ;
    capacityIcon.setFont(digitalfont);
    capacityIcon.setPosition(1105 , 655);
    capacityIcon.setFillColor(sf::Color::Green);
    capacityIcon.setCharacterSize(40);

    sf::SoundBuffer shotBuffer;
    sf::SoundBuffer loolBuffer;
    sf::SoundBuffer endBuffer;
    sf::SoundBuffer shothitBuffer;
    if (!shotBuffer.loadFromFile("images/shot.wav"))
        return -1;
    if (!loolBuffer.loadFromFile("images/loolRotation.wav"))
        return -1;
    if (!endBuffer.loadFromFile("images/END.wav"))
        return -1;
    if (!shothitBuffer.loadFromFile("images/shothit.wav"))
        return -1;
    sf::Sound shot;
    shot.setBuffer(shotBuffer);
    sf::Sound lool;
    lool.setBuffer(loolBuffer);
    lool.setVolume(50);
    sf::Sound End;
    End.setBuffer(endBuffer);
    sf::Sound shothit;
    shothit.setBuffer(shothitBuffer);
    sf::Sprite trumpbody(trumpHills);
    trumpbody.setScale(0,0);


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Start the game loop
    while (app.isOpen())
    {
        // Process events
        sf::Event event;
        while (app.pollEvent(event))
        {

            if(event.type == sf::Event::KeyPressed)
            switch (event.key.code)
            {
                case (sf::Keyboard::Space)://when space key pressed----------------------------
                c=' ';
                shot.play();
                if(shootFlag==0 && trumpmode == false)
                {
                    shootFlag=1;
                    windFlag=0;
                    ydrop=0;
                    basketballydrop=0;
                    basketballFlag=0;
                    basketballHits=0;
                    if(choose==2)
                        poisonCheck=0;
                    else
                        poisonCheck=5;
                    bul.setTexture(options[choose].texture1);
                    bul.setScale(0.025,0.025);
                    damage = options[choose].damage ;
                    if(!turn)
                    {
                        //made first shooting energy
                        float shootx=cos((M_PI/180)*(redLool.getRotation()));
                        float shooty=sin((M_PI/180)*(redLool.getRotation()));
                        if(options[choose].pickt0)
                        {
                            bul.setPosition(-10+(redLool.getPosition().x)+(redLoolLength)*shootx, -10+(redLool.getPosition().y)+(redLoolLength)*shooty);
                            options[choose].sprite1.setTexture(options[choose].texture1);
                            options[choose].capacityt0 -- ;
                            if(options[choose].capacityt0<=0)
                                options[choose].pickt0=false ;
                        }
                    }
                    else
                    {

                        //made first shooting energy
                        float shootx=cos((M_PI/180)*(blueLool.getRotation()));
                        float shooty=sin((M_PI/180)*(blueLool.getRotation()));
                        if(options[choose].pickt1)
                        {
                            //shoot
                            bul.setPosition(-10+(blueLool.getPosition().x)-(blueLoolLength)*shootx, -10+(blueLool.getPosition().y)-(blueLoolLength)*shooty);
                            options[choose].sprite1.setTexture(options[choose].texture1);
                            options[choose].capacityt1 -- ;
                            if(options[choose].capacityt1<=0)
                                options[choose].pickt1=false;
                        }
                    }
                }
                else if(shootFlag==0 && trumpmode == true)
                    {
                        shootFlag=1;
                        windFlag=0;
                        ydrop=0;
                        bul.setTexture(trump.texture1);
                        damage=trump.damage;
                        bul.setScale(0.25 , 0.25);
                        if(!turn)
                        {
                            float shootx=cos((M_PI/180)*(redLool.getRotation()));
                            float shooty=sin((M_PI/180)*(redLool.getRotation()));
                            bul.setPosition(-50+(redLool.getPosition().x)+(redLoolLength)*shootx, -50+(redLool.getPosition().y)+(redLoolLength)*shooty);
                        }else
                        {
                            float shootx=cos((M_PI/180)*(blueLool.getRotation()));
                            float shooty=sin((M_PI/180)*(blueLool.getRotation()));
                            bul.setPosition(-50+(blueLool.getPosition().x)-(blueLoolLength)*shootx, -50+(blueLool.getPosition().y)-(blueLoolLength)*shooty);
                        }
                    }
                break;
                case sf::Keyboard::T :
                    c = 't' ;
                break ;
                case sf::Keyboard::R :
                    if(c == 't')
                        c='r';
                break ;
                case sf::Keyboard::U :
                    if(c == 'r')
                        c='u';
                break ;
                case sf::Keyboard::M :
                    if(c == 'u')
                        c='m';
                break ;
                case sf::Keyboard::P :
                    if( c == 'm'){
                        trumpmode =true ;
                        mus.stop();
                        if (!mus.openFromFile("images/trump.wav"))
                            return -1;
                        mus.play();
                        mus.setLoop(true);
                        endScreen.setTexture(trumpModeEndScreen);
                        endScreen.setScale(1.34,1.13);
                        endtext.setOutlineThickness(10);
                        endtext.setOutlineColor(sf::Color::Black);
                        backdrop.setTexture(trumpModeBackDrop);
                        backdrop.setScale(1,0.5);
                        redLifeValue.setString("");
                        blueLifeValue.setString("");
                        damage = trump.damage ;
                        choose=1;

                        trumpbody.setScale(0.5,0.5);
                        trumpbody.setPosition(540,300);

                        blueLool.setScale(0.5,4);
                        blueLool.move(70,20);
                        blueLoolLength= 286;
                        blueLool.setRotation(75);
                        redLool.setScale(0.5,4);
                        redLool.move(-60,20);
                        redLoolLength=300;
                        redLool.setRotation(285);

                        ban.setTexture(powerbacktex);
                        ban.setScale(0.87 , 0.43);
                        ban.setPosition(720 , 660);


                        trumpModeText.setFont(digitalfont);
                        trumpModeText.setString("TRUMP MODE ENABLED\n   GET HIT = LOOSE!");
                        trumpModeText.setLineSpacing(1.4);
                        trumpModeText.setPosition(740,685);
                        //trumpModeText.setOutlineColor(sf::Color(255,150,150));
                        //trumpModeText.setOutlineThickness(7);
                        trumpModeText.setFillColor(sf::Color::Red);
                        trumpModeText.setCharacterSize(100);
                    }

                break;

                case (sf::Keyboard::PageUp):
                    c=' ';
                    ((turn==0)?PowerR:PowerB)+=1;
                    if(PowerR>=100)
                        PowerR=100;
                    if(PowerB>=100)
                        PowerB=100;
                    break;
                case (sf::Keyboard::PageDown):
                    c=' ';
                    ((turn==0)?PowerR:PowerB)-=1;
                    if(PowerR<=0)
                        PowerR=0;
                    if(PowerB<=0)
                        PowerB=0;
                    break;
                case (sf::Keyboard::Up):
                    c=' ';
                    if(turn==0 && (redLool.getRotation()>285 || redLool.getRotation()<=15))
                    {
                        if(!trumpmode)
                            lool.play();
                        redLool.rotate(-1);
                    }
                    else if(turn==1 && (blueLool.getRotation()>=345 || blueLool.getRotation()<75))
                    {
                        if(!trumpmode)
                            lool.play();
                        blueLool.rotate(1);
                    }
                    break;

                case (sf::Keyboard::Down):
                    c=' ';
                    if(turn==0 && (redLool.getRotation()>=285 || redLool.getRotation()<15))
                    {
                        if(!trumpmode)
                            lool.play();
                        redLool.rotate(1);
                    }
                    else if(turn==1 && (blueLool.getRotation()<=75 || blueLool.getRotation()>345))
                    {
                        if(!trumpmode)
                            lool.play();
                        blueLool.rotate(-1);
                    }
                    break;
                case (sf::Keyboard::Right):
                    c=' ';
                    if(turn==0 && rrdot.getPosition().x<1600)
                    {
                        redTank.move(3,0);
                        redLool.move(3,0);
                        rrdot.move(3,0);
                        lrdot.move(3,0);
                        mrdot.move(3,0);
                    }
                    else if(turn==1 && rbdot.getPosition().x<1600)
                    {
                       blueTank.move(3,0);
                        blueLool.move(3,0);
                        rbdot.move(3,0);
                        lbdot.move(3,0);
                        mbdot.move(3,0);
                    }
                    break;
                case (sf::Keyboard::Left):
                    c=' ';
                    if(turn==0 && lrdot.getPosition().x>0)
                    {
                        redTank.move(-3,0);
                        redLool.move(-3,0);
                        rrdot.move(-3,0);
                        lrdot.move(-3,0);
                        mrdot.move(-3,0);
                    }
                    else if(turn==1 && lbdot.getPosition().x>0)
                    {
                        blueTank.move(-3,0);
                        blueLool.move(-3,0);
                        rbdot.move(-3,0);
                        lbdot.move(-3,0);
                        mbdot.move(-3,0);
                    }
                    break;
                case (sf::Keyboard::LBracket)://left bracket pressed ------------------------------------------------
                    c=' ';
                    choose -- ;
                    if(choose < 0)
                        choose = 3 ;
                    if(!turn)
                    {
                        while(options[choose].pickt0 == false )
                            choose = ((choose == 0)? 3 : (choose - 1));
                    }
                    if(turn)
                    {
                        while(options[choose].pickt1 == false )
                            choose = ((choose == 0)? 3 : (choose - 1));
                    }
                    highlight.setPosition(options[choose].xpo , options[choose].ypo );
                    weaponName.setString(options[choose].name);
                    break ;
                case (sf::Keyboard::RBracket)://Right bracket pressed -----------------------------------------------
                    c= ' ';
                    choose ++ ;
                    if(choose > 3)
                        choose = 0 ;
                    if(!turn)
                    {
                        while(options[choose].pickt0 == false )
                            choose = ((choose == 3)? 0 : (choose + 1));
                    }
                    if(turn)
                    {
                        while(options[choose].pickt1 == false )
                            choose = ((choose == 3)? 0 : (choose + 1));
                    }
                    highlight.setPosition(options[choose].xpo , options[choose].ypo );
                    weaponName.setString(options[choose].name);

                    break ;
                default:
                    break;
            }
            else if (event.type == sf::Event::KeyReleased)
            {
                if(event.key.code == sf::Keyboard::Down)
                    lool.stop();
                else if(event.key.code == sf::Keyboard::Up)
                    lool.stop();
                //else if(event.key.code == sf::Keyboard::Left)
                    //tankmove.stop();
                //else if(event.key.code == sf::Keyboard::Right)
                    //tankmove.stop();
            }
            // Close window : exit
            if (event.type == sf::Event::Closed)
            {
                app.close();
                gameStart=false;
            }
        }
        // Clear screen
        app.clear();
        //gravity red tank
        redTank.move(0,1);
        redLool.move(0,1);
        rrdot.move(0,1);
        lrdot.move(0,1);
        mrdot.move(0,1);
        //stop gravity
        if((Collision::PixelPerfectTest(hills,mrdot)))
        {
            redTank.move(0,-1);
            redLool.move(0,-1);
            rrdot.move(0,-1);
            lrdot.move(0,-1);
            mrdot.move(0,-1);
        }
        //gravity blue tank
        blueTank.move(0,1);
        blueLool.move(0,1);
        rbdot.move(0,1);
        lbdot.move(0,1);
        mbdot.move(0,1);

        //stop gravity
        if((Collision::PixelPerfectTest(hills,mbdot)))
        {
            blueTank.move(0,-1);
            blueLool.move(0,-1);
            rbdot.move(0,-1);
            lbdot.move(0,-1);
            mbdot.move(0,-1);
        }
        //red tank turn
        if(turn==0 && shootFlag==0)
        {
            for(int i=0 ; i<=3 ; i++)
            {
                unlockifitschoosable(options[i].texture1 , options[i].sprite1 , options[i].pickt0);
                if(options[i].pickt0==false)
                        options[i].sprite1.setTexture(options[i].texturelock);
            }

            while(options[choose].pickt0==false)
                 choose=((choose==3)?0:choose+1);

            ss.str("");
            ss << options[choose].name;
            weaponName.setString(ss.str());
            ss.str("");
            ss << options[choose].damage;
            damageIcon.setString(ss.str());
            if(choose!=0)
            {
                ss.str("");
                ss << options[choose].capacityt0;
                capacityIcon.setString(ss.str());
            }
            else
                capacityIcon.setString("infinite");

            highlight.setPosition(options[choose].xpo , options[choose].ypo) ;

            if(windFlag==0)
            {
                windeffect= -3+rand()%7;
                if(windeffect>0)
                {
                    windIcon.setScale(0.35,0.35);
                    ss.str("");
                    ss << windeffect;
                    windValue.setString(ss.str());
                }
                else if(windeffect<0)
                {
                    windIcon.setScale(-0.35,0.35);
                    ss.str("");
                    ss << -windeffect;
                    windValue.setString(ss.str());
                }
                else if(windeffect==0)
                {
                    windIcon.setScale(0,0);
                    ss.str("");
                    ss << windeffect;
                    windValue.setString(ss.str());
                }
                windFlag=1;
            }
            ss.str("");
            ss << PowerR;
            powerValue.setString(ss.str());
            powerBar.setScale(((float)PowerR/222)+0.25, 0.5);

            turnIndicator.setFillColor(sf::Color(255,77,36));
            turnIndicator.setString("RED'S TURN!");

            angle=redLool.getRotation();
            if(angle<=359 && angle>=285)
                angle=360-angle;
            else if(angle>=1 && angle<=15)
                angle*=(-1);

            ss.str("");
            if(angle>=0 && angle<=9)
                ss<<0;
            ss << angle;
            angleValue.setString(ss.str());
            aimback.setFillColor(sf::Color(255,77,36));

            if((Collision::PixelPerfectTest(hills,mrdot))&&(Collision::PixelPerfectTest(rrdot,hills)))
            {
                redTank.move(0,-2);
                redLool.move(0,-2);
                rrdot.move(0,-2);
                lrdot.move(0,-2);
                mrdot.move(0,-2);
            }
            if((Collision::PixelPerfectTest(hills,mrdot))&&(Collision::PixelPerfectTest(lrdot,hills)))
            {
                redTank.move(0,-2);
                redLool.move(0,-2);
                rrdot.move(0,-2);
                lrdot.move(0,-2);
                mrdot.move(0,-2);
            }
        }
        //blue tank turn
        if(turn==1 && shootFlag==0)
        {
            for(int i=0 ; i<=3 ; i++)
            {
                unlockifitschoosable(options[i].texture1 , options[i].sprite1 , options[i].pickt1);
                if(options[i].pickt1==false)
                        options[i].sprite1.setTexture(options[i].texturelock);
            }

            while(options[choose].pickt1==false)
                 choose=((choose==3)?0:choose+1);

            ss.str("");
            ss << options[choose].name;
            weaponName.setString(ss.str());
            ss.str("");
            ss << options[choose].damage;
            damageIcon.setString(ss.str());
            if(choose!=0)
            {
                ss.str("");
                ss << options[choose].capacityt1;
                capacityIcon.setString(ss.str());
            }
            else
                capacityIcon.setString("infinite");

            highlight.setPosition(options[choose].xpo , options[choose].ypo) ;

            if(windFlag==0)
            {
                windeffect= -3+rand()%7;
                if(windeffect>0)
                {
                    windIcon.setScale(0.35,0.35);
                    ss.str("");
                    ss << windeffect;
                    windValue.setString(ss.str());
                }
                else if(windeffect<0)
                {
                    windIcon.setScale(-0.35,0.35);
                    ss.str("");
                    ss << -windeffect;
                    windValue.setString(ss.str());
                }
                else if(windeffect==0)
                {
                    windIcon.setScale(0,0);
                    ss.str("");
                    ss << windeffect;
                    windValue.setString(ss.str());
                }
                windFlag=1;
            }
            ss.str("");
            ss << PowerB;
            powerValue.setString(ss.str());

            powerBar.setScale(((float)PowerB/222)+0.25, 0.5);

            turnIndicator.setFillColor(sf::Color(172,255,110));
            turnIndicator.setString("GREEN'S TURN!");

            angle= blueLool.getRotation();
            if(angle>=345 && angle<=359)
                angle-=360;

            ss.str("");
            if(angle>=0 && angle<=9)
                ss<<0;
            ss << angle;
            angleValue.setString(ss.str());
            aimback.setFillColor(sf::Color(172,255,110));

            if((Collision::PixelPerfectTest(hills,mbdot))&&(Collision::PixelPerfectTest(rbdot,hills)))
            {
                blueTank.move(0,-2);
                blueLool.move(0,-2);
                rbdot.move(0,-2);
                lbdot.move(0,-2);
                mbdot.move(0,-2);
            }
            if((Collision::PixelPerfectTest(hills,mbdot))&&(Collision::PixelPerfectTest(lbdot,hills)))
            {
                blueTank.move(0,-2);
                blueLool.move(0,-2);
                rbdot.move(0,-2);
                lbdot.move(0,-2);
                mbdot.move(0,-2);
            }
        }
        //wind movement
                if(windeffect>0)
                {
                    windFlow.setScale(1,1);
                    if(windFlowFlag==0 || windFlow.getPosition().x>1600)
                    {
                        windFlow.setPosition(-432,100);
                        windFlowFlag=1;
                    }
                    windFlow.move(windeffect,0);
                }
                else if(windeffect<0)
                {
                    windFlow.setScale(-1,1);
                    if(windFlowFlag==0 || windFlow.getPosition().x<0)
                    {
                        windFlow.setPosition(1600+432,100);
                        windFlowFlag=1;
                    }
                    windFlow.move(windeffect,0);
                }
                else if(windeffect==0)
                    windFlow.setScale(0,0);
        // Draw the sprite
        app.draw(backdrop);
        app.draw(windFlow);
        app.draw(hills);
        app.draw(trumpbody);
        app.draw(redLool);
        app.draw(redTank);
        app.draw(blueLool);
        app.draw(blueTank);
        /*app.draw(rrdot);
        app.draw(lrdot);
        app.draw(mrdot);
        app.draw(mbdot);
        app.draw(lbdot);
        app.draw(rbdot);*/

        //when shoot key pressed
        if(shootFlag==1)
        {
            app.draw(bul);
            ydrop+=0.1;
            slowmo=1;
            if(basketballFlag==1)
            {
                if(Collision::PixelPerfectTest(bul,blueTank) || Collision::PixelPerfectTest(bul,redTank))
                {
                    basketballHits++;
                    basketballydrop=0;
                    if(basketballHits==3)
                        basketballFlag=0;
                }
            }
            if(basketballFlag==1)
            {
                basketballydrop+=0.3;
                bul.move(0,-15+basketballydrop);
            }
            if(Collision::PixelPerfectTest(bul,blueTank) || (Collision::PixelPerfectTest(bul,blueLool) && trumpmode==true && turn==0))
                {
                    if(choose==3)
                        blueLife-=(damage/4);
                    else if(choose==2)
                    {
                        blueLife-=(damage/5);
                        app.draw(bul);
                        app.draw(redLifeValue);
                        app.draw(blueLifeValue);
                        app.draw(hub);
                        app.draw(metalplate);
                        app.draw(powerbarshadow);
                        app.draw(powerBar);
                        app.draw(powerValue);
                        app.draw(aimback);
                        app.draw(aimlogo);
                        app.draw(turnIndicator);
                        app.draw(angleValue);
                        app.draw(windIcon);
                        app.draw(windValue);
                        app.draw(backRect);
                        app.draw(damageIcon);
                        app.draw(damageName);
                        app.draw(weaponName);
                        app.draw(capacityIcon);
                        for(int i=0 ; i< 4 ; i++)
                            app.draw(backDefaultS[i]);
                        app.draw(highlight);
                        for(int i=0 ; i<4 ; i++)
                        app.draw(options[i].sprite1);

                        app.display();
                        delay(500);
                        poisonCheck++;
                    }
                    else
                        blueLife-=damage;

                    //basketball move
                    if(choose==3 && basketballHits<3)
                        basketballFlag=1;
                    //...
                    if(blueLife<80 && blueLife>=60)
                        blueTank.setTexture(blue1Tex);
                    else if(blueLife<60 && blueLife>=40)
                        blueTank.setTexture(blue2Tex);
                    else if(blueLife<40 && blueLife>=20)
                        blueTank.setTexture(blue3Tex);
                    else if(blueLife<20 && blueLife>0)
                        blueTank.setTexture(blue4Tex);

                    //basketball move
                    else if(blueLife<=0)
                    {
                        End.play();
                        blueLife=0;
                        blueTank.setTexture(endexplosion);
                        if(trumpmode==true)
                        {
                            blueTank.setScale(3,3);
                            blueTank.move(-250,-250);
                        }
                        else
                            blueTank.setScale(1.5,1.5);
                        blueTank.move(-150,-200);
                        app.draw(blueTank);
                        app.display();
                        delay(1000);
                            endtext.setFillColor(sf::Color::Red);
                            endtext.setString("RED WINS");
                                for(int i=20 ; i<200 ; i++){
                                    endtext.setCharacterSize(i);
                                    endtext.setOrigin(endtext.getLocalBounds().width /2 , endtext.getLocalBounds().height /2);
                                    endtext.setPosition(800,365);
                                    app.draw(endScreen);
                                    app.draw(endtext);
                                    app.display();
                                    delay(5);
                                }
                                delay(1400);
                                app.close();
                                gameStart=false;
                    }
                    ss.str("");
                    ss << blueLife;
                    blueLifeValue.setString(ss.str());

                    if(basketballFlag==0 && poisonCheck==5)
                    {
                        shothit.play();
                        if(trumpmode)
                            bul.setTexture(explosion1);
                        else if(choose==2)
                            bul.setTexture(acidexplosion);
                        else if(choose==0)
                            bul.setTexture(discoballexplosion);
                        else
                            bul.setTexture(explosion1);
                        bul.setScale(0.25,0.25);
                        bul.move(-75,-70);
                    }
                    app.draw(bul);
                    app.draw(redLifeValue);
                    app.draw(blueLifeValue);
                    app.draw(hub);
                    app.draw(metalplate);
                    app.draw(powerbarshadow);
                    app.draw(powerBar);
                    app.draw(powerValue);
                    app.draw(aimback);
                    app.draw(aimlogo);
                    app.draw(turnIndicator);
                    app.draw(angleValue);
                    app.draw(windIcon);
                    app.draw(windValue);
                    app.draw(backRect);
                    app.draw(damageIcon);
                    app.draw(damageName);
                    app.draw(weaponName);
                    app.draw(capacityIcon);
                    for(int i=0 ; i< 4 ; i++)
                        app.draw(backDefaultS[i]);
                    app.draw(highlight);
                    for(int i=0 ; i<4 ; i++)
                    app.draw(options[i].sprite1);

                    app.display();

                    if(basketballFlag==0 && poisonCheck==5)
                    {
                        delay(500);
                        shootFlag=0;
                        turn=((turn==0)?1:0);
                        windFlowFlag=0;
                        bul.setPosition(0,0);
                    }
                }
            else if(Collision::PixelPerfectTest(bul,redTank) || (Collision::PixelPerfectTest(bul,redLool) && trumpmode==true && turn==1))
                {
                    if(choose==3)
                        redLife-=(damage/4);
                    else if(choose==2)
                    {
                        redLife-=(damage/5);
                        app.draw(bul);
                        app.draw(redLifeValue);
                        app.draw(blueLifeValue);
                        app.draw(hub);
                        app.draw(metalplate);
                        app.draw(powerbarshadow);
                        app.draw(powerBar);
                        app.draw(powerValue);
                        app.draw(aimback);
                        app.draw(aimlogo);
                        app.draw(turnIndicator);
                        app.draw(angleValue);
                        app.draw(windIcon);
                        app.draw(windValue);
                        app.draw(backRect);
                        app.draw(damageIcon);
                        app.draw(damageName);
                        app.draw(weaponName);
                        app.draw(capacityIcon);
                        for(int i=0 ; i< 4 ; i++)
                            app.draw(backDefaultS[i]);
                        app.draw(highlight);
                        for(int i=0 ; i<4 ; i++)
                        app.draw(options[i].sprite1);

                        app.display();
                        delay(500);
                        poisonCheck++;
                    }
                    else
                        redLife-=damage;

                    if(choose==3 && basketballHits<3)
                        basketballFlag=1;

                    if(redLife<80 && redLife>=60)
                        redTank.setTexture(red1Tex);
                    else if(redLife<60 && redLife>=40)
                        redTank.setTexture(red2Tex);
                    else if(redLife<40 && redLife>=20)
                        redTank.setTexture(red3Tex);
                    else if(redLife<20 && redLife>0)
                        redTank.setTexture(red4Tex);

                    else if(redLife<=0)
                    {
                        End.play();
                        redLife=0;
                        redTank.setTexture(endexplosion);
                        if(trumpmode==true)
                        {
                            redTank.setScale(3,3);
                            redTank.move(-250,-250);
                        }
                        else
                            redTank.setScale(1.5,1.5);
                        redTank.move(-150,-200);
                        app.draw(redTank);
                        app.display();
                        delay(1000);
                            endtext.setFillColor(sf::Color::Green);
                            endtext.setString("GREEN WINS");
                                for(int i=20 ; i<200 ; i++){
                                    endtext.setCharacterSize(i);
                                    endtext.setOrigin(endtext.getLocalBounds().width /2 , endtext.getLocalBounds().height /2);
                                    endtext.setPosition(800,365);
                                    app.draw(endScreen);
                                    app.draw(endtext);
                                    app.display();
                                    delay(5);
                                }
                                delay(1400);
                                gameStart=false;
                                app.close();
                    }
                    ss.str("");
                    ss << redLife;
                    redLifeValue.setString(ss.str());

                    if(basketballFlag==0 && poisonCheck==5)
                    {
                        shothit.play();
                        if(trumpmode)
                            bul.setTexture(explosion1);
                        else if(choose==2)
                            bul.setTexture(acidexplosion);
                        else if(choose==0)
                            bul.setTexture(discoballexplosion);
                        else
                            bul.setTexture(explosion1);
                        bul.setScale(0.25,0.25);
                        bul.move(-75,-70);
                    }

                    app.draw(bul);
                    app.draw(redLifeValue);
                    app.draw(blueLifeValue);
                    app.draw(hub);
                    app.draw(metalplate);
                    app.draw(powerbarshadow);
                    app.draw(powerBar);
                    app.draw(powerValue);
                    app.draw(aimback);
                    app.draw(aimlogo);
                    app.draw(turnIndicator);
                    app.draw(angleValue);
                    app.draw(windIcon);
                    app.draw(windValue);
                    app.draw(backRect);
                    app.draw(damageIcon);
                    app.draw(damageName);
                    app.draw(weaponName);
                    app.draw(capacityIcon);
                    for(int i=0 ; i< 4 ; i++)
                        app.draw(backDefaultS[i]);
                    app.draw(highlight);
                    for(int i=0 ; i<4 ; i++)
                    app.draw(options[i].sprite1);

                    app.display();

                    if(basketballFlag==0 && poisonCheck==5)
                    {
                        delay(500);
                        shootFlag=0;
                        turn=((turn==0)?1:0);
                        windFlowFlag=0;
                        bul.setPosition(0,0);
                    }
                }
            else if(Collision::PixelPerfectTest(bul,hills))
                {
                    if(trumpmode)
                        bul.setTexture(explosion1);
                    else if(choose==2)
                        bul.setTexture(acidexplosion);
                    else if(choose==0)
                        bul.setTexture(discoballexplosion);
                    else
                        bul.setTexture(explosion1);
                    if(trumpmode==true)
                        bul.setScale(0.45,0.45);
                    else
                        bul.setScale(0.25,0.25);
                    bul.move(-75,-70);

                    app.draw(bul);
                    app.draw(redLifeValue);
                    app.draw(blueLifeValue);
                    app.draw(hub);
                    app.draw(metalplate);
                    app.draw(powerbarshadow);
                    app.draw(powerBar);
                    app.draw(powerValue);
                    app.draw(aimback);
                    app.draw(aimlogo);
                    app.draw(turnIndicator);
                    app.draw(angleValue);
                    app.draw(windIcon);
                    app.draw(windValue);
                    app.draw(backRect);
                    app.draw(damageIcon);
                    app.draw(damageName);
                    app.draw(weaponName);
                    app.draw(capacityIcon);
                    for(int i=0 ; i< 4 ; i++)
                        app.draw(backDefaultS[i]);
                    app.draw(highlight);
                    for(int i=0 ; i<4 ; i++)
                    app.draw(options[i].sprite1);

                    app.display();
                    delay(500);

                    shootFlag=0;
                    turn=((turn==0)?1:0);
                    windFlowFlag=0;
                }
            else if(bul.getPosition().x >app.getSize().x || bul.getPosition().x <0)
                {
                    shootFlag=0;
                    turn=((turn==0)?1:0);
                    windFlowFlag=0;
                }
            else if(turn==0)
            {
                power=(PowerR*15/100);
                float shootx=cos((M_PI/180)*(redLool.getRotation()));
                float shooty=sin((M_PI/180)*(redLool.getRotation()));
                if(trumpmode==true)
                {
                    slowmo=3;
                    ydrop-=0.065;
                }

                if(basketballFlag==0)
                    bul.move((power*shootx + windeffect)/slowmo, (ydrop + power*shooty)/slowmo);
            }
            else if(turn==1)
            {
                power=(PowerB*15/100);
                float shootx=cos((M_PI/180)*(blueLool.getRotation()));
                float shooty=sin((M_PI/180)*(blueLool.getRotation()));
                if(trumpmode==true)
                {
                    slowmo=3;
                    ydrop-=0.065;
                }
                if(basketballFlag==0)
                    bul.move((-power*shootx + windeffect)/slowmo, (ydrop - power*shooty)/slowmo);
            }
        }
        app.draw(redLifeValue);
        app.draw(blueLifeValue);
        app.draw(hub);
        app.draw(metalplate);
        app.draw(powerbarshadow);
        app.draw(powerBar);
        app.draw(powerValue);
        //app.draw(bullogo);
        app.draw(aimback);
        app.draw(aimlogo);
        app.draw(turnIndicator);
        app.draw(angleValue);
        app.draw(windIcon);
        app.draw(windValue);
        app.draw(backRect);
        app.draw(damageIcon);
        app.draw(damageName);
        app.draw(weaponName);
        app.draw(capacityIcon);
        for(int i=0 ; i< 4 ; i++)
            app.draw(backDefaultS[i]);
        app.draw(highlight);
        for(int i=0 ; i<4 ; i++)
            app.draw(options[i].sprite1);
        app.draw(ban);
        app.draw(trumpModeText);

        // Update the window
        app.display();
    }
    }
    }
}
void delay(int delayTime)
{
    sf::Clock clock;
    sf::Time elapsed;
    do{
        elapsed = clock.getElapsedTime();
    }while (elapsed.asMilliseconds() < delayTime);
    clock.restart();
}
void unlockifitschoosable (sf::Texture &t , sf::Sprite &s , bool p){
    if(p)
        s.setTexture(t);
}

